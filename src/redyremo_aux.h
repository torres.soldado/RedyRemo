/*
 * redyremo_aux.h
 *
 *  Created on: 16 Dec 2014
 *      Author: keeper
 */

#ifndef REDYREMO_AUX_H_
#define REDYREMO_AUX_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <netinet/in.h>
#include "miniwget.h"
#include "miniupnpc.h"
#include "upnpcommands.h"
#include "upnperrors.h"

const char *
protofix(const char * proto);

void
DisplayInfos(struct UPNPUrls * urls, struct IGDdatas * data);

void
GetConnectionStatus(struct UPNPUrls * urls, struct IGDdatas * data);

void
ListRedirections(struct UPNPUrls * urls, struct IGDdatas * data);

void
NewListRedirections(struct UPNPUrls * urls, struct IGDdatas * data);

int
SetRedirectAndTest(struct UPNPUrls * urls,
                   struct IGDdatas * data,
                   const char * iaddr,
                   const char * iport,
                   const char * eport,
                   const char * proto,
                   const char * leaseDuration,
                   const char * description,
                   int addAny);

int
RemoveRedirect(struct UPNPUrls * urls,
               struct IGDdatas * data,
               const char * eport,
               const char * proto,
               const char * remoteHost);

void
RemoveRedirectRange(struct UPNPUrls * urls,
                    struct IGDdatas * data,
                    const char * ePortStart,
                    char const * ePortEnd,
                    const char * proto,
                    const char * manage);

void
GetFirewallStatus(struct UPNPUrls * urls, struct IGDdatas * data);

void
SetPinholeAndTest(struct UPNPUrls * urls,
                  struct IGDdatas * data,
                  const char * remoteaddr,
                  const char * eport,
                  const char * intaddr,
                  const char * iport,
                  const char * proto,
                  const char * lease_time);

void
GetPinholeAndUpdate(struct UPNPUrls * urls,
                    struct IGDdatas * data,
                    const char * uniqueID,
                    const char * lease_time);

void
GetPinholeOutboundTimeout(struct UPNPUrls * urls,
                          struct IGDdatas * data,
                          const char * remoteaddr,
                          const char * eport,
                          const char * intaddr,
                          const char * iport,
                          const char * proto);

void
GetPinholePackets(struct UPNPUrls * urls,
                  struct IGDdatas * data,
                  const char * uniqueID);

void
CheckPinhole(struct UPNPUrls * urls,
             struct IGDdatas * data,
             const char * uniqueID);

void
RemovePinhole(struct UPNPUrls * urls,
              struct IGDdatas * data,
              const char * uniqueID);

#endif /* REDYREMO_AUX_H_ */
