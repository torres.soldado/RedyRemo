/*
 * server.cpp
 *
 *  Created on: 19 Dec 2014
 *      Author: keeper
 */

#include <unistd.h>
#include <string>
#include <sstream>
#include <vector>
#include <mutex>
#include <algorithm>
#include <boost/asio.hpp>
#include <boost/date_time.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

using boost::asio::ip::udp;

/**
 * Holds information for a single client:
 *  - External port.
 *  - Public IP address.
 *  - MAC address, we use this to ID the device.
 */
struct client {
  boost::posix_time::ptime time_of_last_announce;
  std::string port;
  std::string ip;
  std::string mac;

  /*
   * So we can use stl find.
   */
  bool
  operator==(const client& rhs) const
  {
    if (this->mac.compare(rhs.mac) == 0)
      return true;
    else
      return false;
  }

  /*
   * So we can use stl find.
   */
  bool
  operator!=(const client& rhs) const
  {
    if (this->mac.compare(rhs.mac) == 0)
      return false;
    else
      return true;
  }

  friend std::ostream&
  operator<<(std::ostream& os, const client& rhs);
};

std::ostream&
operator<<(std::ostream& os, const client& rhs)
{
  boost::posix_time::ptime now =
                boost::posix_time::microsec_clock::universal_time();
  boost::posix_time::time_duration d = rhs.time_of_last_announce
                  - now;

  os << rhs.mac << " Seen alive " << d << " ago at " << rhs.ip << ":" << rhs.port;
  return os;
}

/**
 * Receives "notification" packets from clients.
 */
class udp_server {
public:
  udp_server(boost::asio::io_service& io_service)
          : _socket(io_service, udp::endpoint(udp::v4(), 55555)),
            _thread(boost::bind(&udp_server::worker, this))
  {
    start_receive();
  }

private:
  udp::socket _socket;
  udp::endpoint _remote_endpoint;
  boost::array<char, 2048> _recv_buffer;
  std::vector<client> _clients;
  std::mutex _clients_mutex;
  boost::thread _thread;

  void
  worker()
  {
    std::cout << "Starting worker" << std::endl;

    while (true) {
      _clients_mutex.lock();
      std::cout << "\033[2J\033[1;1H";  // clear terminal
      std::cout << "A total of " << _clients.size()
              << " clients have announced to date." << std::endl;
      for (client client_ : _clients) {
        std::cout << client_ << std::endl;
      }
      _clients_mutex.unlock();

      boost::this_thread::sleep(boost::posix_time::seconds(1));
    }
  }

  void
  start_receive()
  {
    _socket.async_receive_from(
            boost::asio::buffer(_recv_buffer),
            _remote_endpoint,
            boost::bind(&udp_server::handle_receive, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
  }

  void
  insert_client(client client_)
  {
    _clients_mutex.lock();

    std::vector<client>::iterator it = std::find(_clients.begin(),
                                                 _clients.end(), client_);
    if (it == _clients.end()) {
      // New client
      _clients.push_back(client_);
    } else {
      // Existing client
      (*it).time_of_last_announce = client_.time_of_last_announce;
    }

    _clients_mutex.unlock();
  }

  void
  handle_receive(const boost::system::error_code& error,
                 std::size_t bytes_transferred)
  {
    if (error) {
      std::cout << "Error receiving message.\n";
      return;
    }

    // get client info
    std::string data(_recv_buffer.begin(),
                     _recv_buffer.begin() + bytes_transferred);
    std::cout << "Received data: " << data << std::endl;

    std::string::size_type begin_mac = data.find("mac=") + 4;
    std::string::size_type end_mac = data.find(";", begin_mac);
    std::string mac = data.substr(begin_mac, end_mac - begin_mac);
    std::cout << "mac is " << mac << std::endl;

    std::string::size_type begin_ip = data.find("external_ip=") + 12;
    std::string::size_type end_ip = data.find(";", begin_ip);
    std::string ip = data.substr(begin_ip, end_ip - begin_ip);
    std::cout << "ip is " << ip << std::endl;

    std::string::size_type begin_port = data.find("external_port=") + 14;
    std::string::size_type end_port = data.find(";", begin_port);
    std::string port = data.substr(begin_port, end_port - begin_port);
    std::cout << "port is " << port << std::endl;

    client client_;
    client_.ip = ip;
    client_.mac = mac;
    client_.port = port;
    client_.time_of_last_announce =
            boost::posix_time::microsec_clock::universal_time();
    insert_client(client_);

    start_receive();
  }

  void
  handle_send(boost::shared_ptr<std::string> /*message*/,
              const boost::system::error_code&/*error*/,
              std::size_t /*bytes_transferred*/)
  {
  }
};

int
main(int argc, char ** argv)
{
  try {
    boost::asio::io_service io_service;
    udp_server server(io_service);
    io_service.run();
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
  return 0;
}
