################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/connecthostport.c \
../src/igd_desc_parse.c \
../src/minisoap.c \
../src/minissdpc.c \
../src/miniupnpc.c \
../src/miniwget.c \
../src/minixml.c \
../src/portlistingparse.c \
../src/receivedata.c \
../src/redyremo_aux.c \
../src/upnpcommands.c \
../src/upnperrors.c \
../src/upnpreplyparse.c 

CPP_SRCS += \
../src/RedyRemo.cpp 

OBJS += \
./src/RedyRemo.o \
./src/connecthostport.o \
./src/igd_desc_parse.o \
./src/minisoap.o \
./src/minissdpc.o \
./src/miniupnpc.o \
./src/miniwget.o \
./src/minixml.o \
./src/portlistingparse.o \
./src/receivedata.o \
./src/redyremo_aux.o \
./src/upnpcommands.o \
./src/upnperrors.o \
./src/upnpreplyparse.o 

C_DEPS += \
./src/connecthostport.d \
./src/igd_desc_parse.d \
./src/minisoap.d \
./src/minissdpc.d \
./src/miniupnpc.d \
./src/miniwget.d \
./src/minixml.d \
./src/portlistingparse.d \
./src/receivedata.d \
./src/redyremo_aux.d \
./src/upnpcommands.d \
./src/upnperrors.d \
./src/upnpreplyparse.d 

CPP_DEPS += \
./src/RedyRemo.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	arm926ejs-g++ -I/home/keeper/tools/cpp/boost_install_armv5/include -Os -Wall -c -fmessage-length=0 -std=c++0x -D_BSD_SOURCE -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	arm926ejs-gcc -Os -Wall -c -fmessage-length=0 -std=gnu99 -D_BSD_SOURCE -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


